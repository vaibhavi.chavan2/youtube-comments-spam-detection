import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    wordfilename,count=line.split('\t',1)
    word,filename=wordfilename.split(' ',1)
    z=word+' '+count;
    print '%s\t%s' % (filename, z)